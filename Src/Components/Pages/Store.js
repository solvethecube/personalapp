import React, { Component } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, Image, TextInput, Dimensions, FlatList, Modal, ImageBackground } from 'react-native';
import CONFIGURATION from '../../Reducer/Unills/Config';
import style from './../../Reducer/Styles/Store'
import GeneralStatusBar from './../GeneralStatusBar'
import ShopView from './../ShopView'
import PagerView from 'react-native-pager-view';

const { width } = Dimensions.get("screen")
class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Ex: [{ type:"Shop", color: "#FF725E", title: "Raj Moti Store", url: "https://image.shutterstock.com/image-photo/shopping-kids-during-virus-outbreak-260nw-1677576121.jpg" }, {type:"Offer", color: "#A47200", title: "Raj Moti Store", url: "https://media-cdn.tripadvisor.com/media/photo-s/06/17/c7/3f/item-shop.jpg" }, { type:"Offer",color: "#A0616A", title: "Raj Moti Store", url: "https://media-cdn.tripadvisor.com/media/photo-s/06/17/c7/3f/item-shop.jpg" }, { type:"More",color: "#735BF6", title: "Raj Moti Store", url: "https://image.shutterstock.com/image-photo/shopping-kids-during-virus-outbreak-260nw-1677576121.jpg" }],
            modalVisible: false,
            pageIndex: 0
        };
    }

    render() {
        return (
            <View style={style.container}>
                <GeneralStatusBar backgroundColor={CONFIGURATION.PrimaryColor} barStyle="light-content" />
                <View style={style.headerView}>
                    <TouchableOpacity onPress={() => { }}>
                        <Image resizeMode={"contain"} style={style.menuImage} source={require("./../Images/Back_W.png")} />
                    </TouchableOpacity>
                </View>
                <View style={style.titleView}>
                    <Text style={style.headerText}>Freshbuy Stores</Text>
                </View>
                <Text style={{ color: CONFIGURATION.White + 50, marginHorizontal: 20, fontFamily: CONFIGURATION.TextRegular }}>#16/4, 4th Cross, 10th Main, BTM Layout,
                    Stage-2, Bengaluru 560 100</Text>
                <TouchableOpacity onPress={()=>{this.props.navigation.navigate("StoreImage")}}>
                    <Text style={{ color: CONFIGURATION.lightBlue, marginHorizontal: 20, fontFamily: CONFIGURATION.TextRegular }}>Store offers home delivery within 3km </Text>
                </TouchableOpacity>

                <View style={style.filertView}>
                    <TouchableOpacity>
                        <Image resizeMode={"contain"} style={{ height: 25, width: 25 }} source={require("./../Images/location_W.png")} />
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Image resizeMode={"contain"} style={{ height: 25, width: 25 }} source={require("./../Images/phone_call.png")} />
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Image resizeMode={"contain"} style={{ height: 25, width: 25 }} source={require("./../Images/whatsapp.png")} />
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Image resizeMode={"contain"} style={{ height: 25, width: 25 }} source={require("./../Images/message.png")} />
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Image resizeMode={"contain"} style={{ height: 25, width: 25 }} source={require("./../Images/like.png")} />
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Image resizeMode={"contain"} style={{ height: 25, width: 25 }} source={require("./../Images/favourite_F.png")} />
                    </TouchableOpacity>
                </View>
                <View style={{ flexDirection: "row", }}>
                    <TouchableOpacity onPress={()=>{this.setState({pageIndex:0})}} style={{ paddingVertical: 15, borderColor: this.state.pageIndex == 0 ? CONFIGURATION.lightBlue : CONFIGURATION.White, borderBottomWidth: 2, width: "50%" }} >
                        <Text style={{ color: this.state.pageIndex == 0 ? CONFIGURATION.lightBlue : CONFIGURATION.White, textAlign: "center", fontFamily: CONFIGURATION.TextBold }}>PRODUCTS</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>{this.setState({pageIndex:1})}} style={{ paddingVertical: 15, borderColor: this.state.pageIndex == 1 ? CONFIGURATION.lightBlue : CONFIGURATION.White, borderBottomWidth: 2, width: "50%" }} >
                        <Text style={{ color: this.state.pageIndex == 1 ? CONFIGURATION.lightBlue : CONFIGURATION.White, textAlign: "center", fontFamily: CONFIGURATION.TextBold }}>CATALOGS</Text>
                    </TouchableOpacity>
                </View>
                <PagerView style={{ flex: 1 }} initialPage={this.state.pageIndex} onPageSelected={(e) => { this.setState({ pageIndex: e.nativeEvent.position }) }} >
                    <View key="1">
                        <View style={style.searchView}>
                            <TextInput
                                style={style.input}
                                placeholder="Search"
                                placeholderTextColor={CONFIGURATION.White + 50}
                            />
                            <Image resizeMode={"contain"} style={style.searchImage} source={require("./../Images/Search_G.png")} />
                        </View>
                        <FlatList
                            data={this.state.Ex}
                            style={{ marginHorizontal: 10, }}
                            numColumns={2}
                            renderItem={({ item }) => {
                                return (
                                    <ShopView onPress={() => { this.setState({ modalVisible: true }) }} type={item.type} color={item.color} diss={"26"} Url={item.url} title={item.title} />
                                )
                            }}
                            keyExtractor={item => item.id}
                        />
                    </View>
                    <View key="2">
                    <View style={style.searchView}>
                            <TextInput
                                style={style.input}
                                placeholder="Search"
                                placeholderTextColor={CONFIGURATION.White + 50}
                            />
                            <Image resizeMode={"contain"} style={style.searchImage} source={require("./../Images/Search_G.png")} />
                        </View>
                        <FlatList
                            data={this.state.Ex}
                            style={{ marginHorizontal: 10, }}
                            numColumns={2}
                            renderItem={({ item }) => {
                                return (
                                    <ShopView onPress={() => { this.setState({ modalVisible: true }) }} type={item.type} color={item.color} diss={"26"} Url={item.url} title={item.title} />
                                )
                            }}
                            keyExtractor={item => item.id}
                        />
                    </View>
                </PagerView>

                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                    }}
                >
                    <View style={style.centeredView}>
                        <View style={style.modalView}>
                            <TouchableOpacity onPress={() => { this.setState({ modalVisible: false }) }} style={{ width: 25, alignSelf: "flex-end", margin: 10 }}>
                                <Image style={{ height: 25, width: 25 }} source={require("./../Images/Close_W.png")} />
                            </TouchableOpacity>
                            <View style={{ alignItems: "center", marginTop: -10 }}>
                                <ShopView onPress={() => { }} type={"Offer"} color={"#FF725E"} diss={"26"} Url={this.state.Ex[0].url} title={this.state.Ex[0].title} />
                            </View>
                            <Text style={{ color: CONFIGURATION.lightBlue, fontFamily: CONFIGURATION.TextRegular, marginHorizontal: 35, textAlign: "center", marginVertical: 10 }}>₹56,000 ₹45,900
                                {"\n"}Adptor charger free on SBI Credit Card Payment</Text>
                            <View style={{ height: 1, marginHorizontal: 30, backgroundColor: CONFIGURATION.White + 50, }}></View>
                            <Text style={{ fontSize: 16, marginTop: 10, fontFamily: CONFIGURATION.TextBold, color: CONFIGURATION.White + 90, textAlign: "center" }}>Poorvika Mobiles</Text>
                            <Text style={{ color: CONFIGURATION.White + 50, fontFamily: CONFIGURATION.TextRegular, marginHorizontal: 35, textAlign: "center", marginVertical: 10 }}>Neeladri Road, Electronic City, Phase-1
                                Bengaluru 560 100</Text>
                            <Text style={{ color: CONFIGURATION.lightBlue, fontFamily: CONFIGURATION.TextRegular, marginHorizontal: 35, textAlign: "center", marginTop: 10 }}>Store offers home delivery within 3km </Text>
                            <View style={{ flexDirection: "row", alignItems: "center", alignSelf: "center", marginVertical: 20, }}>
                                <TouchableOpacity>
                                    <Image style={{ height: 25, width: 25, }} resizeMode="contain" source={require("./../Images/phone_call.png")} />
                                </TouchableOpacity>
                                <TouchableOpacity style={{ height: 50, width: "40%", backgroundColor: CONFIGURATION.lightBlue, marginHorizontal: 20, alignItems: "center", justifyContent: "center", borderRadius: 10 }} >
                                    <Text style={{ fontFamily: CONFIGURATION.TextMedium, color: CONFIGURATION.White, textAlign: "center" }} >GO TO STORE</Text>
                                </TouchableOpacity>
                                <TouchableOpacity>
                                    <Image style={{ height: 25, width: 25, }} resizeMode="contain" source={require("./../Images/whatsapp.png")} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
}

export default App