import React, { Component } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, Image, TextInput, Dimensions, FlatList, Modal, ImageBackground, ScrollView } from 'react-native';
import CONFIGURATION from '../../Reducer/Unills/Config';
import style from './../../Reducer/Styles/StoreImage'
import GeneralStatusBar from './../GeneralStatusBar'
import ShopView from './../ShopView'
import PagerView from 'react-native-pager-view'; 
const { width ,height} = Dimensions.get("screen")
class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Ex: [{ type: "Shop", color: "#FF725E", title: "Raj Moti Store", url: "https://image.shutterstock.com/image-photo/shopping-kids-during-virus-outbreak-260nw-1677576121.jpg" }, { type: "Offer", color: "#A47200", title: "Raj Moti Store", url: "https://media-cdn.tripadvisor.com/media/photo-s/06/17/c7/3f/item-shop.jpg" }, { type: "Offer", color: "#A0616A", title: "Raj Moti Store", url: "https://media-cdn.tripadvisor.com/media/photo-s/06/17/c7/3f/item-shop.jpg" }, { type: "More", color: "#735BF6", title: "Raj Moti Store", url: "https://image.shutterstock.com/image-photo/shopping-kids-during-virus-outbreak-260nw-1677576121.jpg" }],
            modalVisible: false,
            pageIndex: 0,
            url:"https://trak.in/wp-content/uploads/2020/10/images-2020-10-24T153811.037-2.jpeg"
        };
    }

    render() {
        return (
            <View style={style.container}>
                <GeneralStatusBar backgroundColor={CONFIGURATION.PrimaryColor} barStyle="light-content" />
                <View style={style.headerView}>
                    <TouchableOpacity onPress={() => { }}>
                        <Image resizeMode={"contain"} style={style.menuImage} source={require("./../Images/Back_W.png")} />
                    </TouchableOpacity>
                    <View style={{ flexDirection: "row", alignItems: "center", width: "20%", justifyContent: "space-between" }}>
                        <TouchableOpacity onPress={() => { }}>
                            <Image resizeMode={"contain"} style={style.menuImage} source={require("./../Images/Edit_Message.png")} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => { }}>
                            <Image resizeMode={"contain"} style={style.menuImage} source={require("./../Images/share.png")} />
                        </TouchableOpacity>
                    </View>
                </View>
                <ScrollView>
                <View style={style.titleView}>
                    <Text style={style.headerText}>Freshbuy Stores</Text>
                </View>
                <Text style={{ color: CONFIGURATION.White + 50, marginHorizontal: 20, fontFamily: CONFIGURATION.TextRegular }}>Ganesha Chaturthi Offer on Home Items{`\n`}Valid Till : 30-Nov-2020</Text>
                <View style={{height:1,backgroundColor: CONFIGURATION.White+50,marginVertical: 10,}}></View>
                <Image resizeMode={"cover"} style={{height:height/1.7,width:width}} source={{uri:this.state.url}} />
                <FlatList
                    data={this.state.Ex}
                    style={{ marginVertical: 10, }}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    renderItem={({ item }) => {
                        return (
                            <TouchableOpacity onPress={()=>{this.setState({url:item.url})}}>
                                <Image style={{height:70,width:70,marginHorizontal: 5,}} source={{uri:item.url}}/>
                            </TouchableOpacity>
                        )
                    }}
                    keyExtractor={item => item.id}
                />
                </ScrollView>
            </View>
        );
    }
}

export default App