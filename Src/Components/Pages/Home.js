import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity, TextInput,FlatList } from 'react-native';
import CONFIGURATION from '../../Reducer/Unills/Config';
import style from './../../Reducer/Styles/HomeStyle'
import GeneralStatusBar from './../GeneralStatusBar'
import ShopView from './../ShopView'
class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Ex: [{title:"Raj Moti Store",url:"https://image.shutterstock.com/image-photo/shopping-kids-during-virus-outbreak-260nw-1677576121.jpg"},{title:"Raj Moti Store",url:"https://media-cdn.tripadvisor.com/media/photo-s/06/17/c7/3f/item-shop.jpg"},{title:"Raj Moti Store",url:"https://media-cdn.tripadvisor.com/media/photo-s/06/17/c7/3f/item-shop.jpg"},{title:"Raj Moti Store",url:"https://image.shutterstock.com/image-photo/shopping-kids-during-virus-outbreak-260nw-1677576121.jpg"}]
        };
    }

    render() {
        return (
            <View style={style.container}>
                <GeneralStatusBar backgroundColor={CONFIGURATION.PrimaryColor} barStyle="light-content" />
                <View style={style.headerView}>
                    <TouchableOpacity onPress={() => { }}>
                        <Image resizeMode={"contain"} style={style.menuImage} source={require("./../Images/Back_W.png")} />
                    </TouchableOpacity>
                </View>
                <View style={style.titleView}>
                    <Text style={style.headerText}>Stores Near Me</Text>
                    <TouchableOpacity style={style.locationView} >
                        <Image resizeMode={"contain"} style={style.locationImage} source={require("./../Images/location.png")} />
                        <Text style={style.textlocation}>Current Location</Text>
                    </TouchableOpacity>
                </View>
                <View style={style.searchView}>
                    <TextInput
                        style={style.input}
                        placeholder="Search"
                        placeholderTextColor={CONFIGURATION.White + 50}
                    />
                    <Image resizeMode={"contain"} style={style.searchImage} source={require("./../Images/Search_G.png")} />
                </View>
                <View style={style.filertView}>
                    <TouchableOpacity style={{ flexDirection: "row", alignItems: "center" }}>
                        <Text style={style.texts}>Store Categories</Text>
                        <Image resizeMode={"contain"} style={style.filterimage} source={require("./../Images/Filter.png")} />
                    </TouchableOpacity>
                    <TouchableOpacity style={{ flexDirection: "row", alignItems: "center" }}>
                        <Text style={style.texts}>Favourites</Text>
                        <Image resizeMode={"contain"} style={style.filterimage} source={require("./../Images/hart.png")} />
                    </TouchableOpacity>
                </View>
                <FlatList
                    data={this.state.Ex}
                    style={{marginHorizontal: 10,}}
                    numColumns={2}
                    renderItem={({item})=>{
                        return(
                            <ShopView type={"Shop"} Url={item.url} title={item.title}/>            
                        )
                    }}
                    keyExtractor={item => item.id}
                />
            </View>
        );
    }
}

export default App