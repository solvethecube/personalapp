import React, { Component } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, Image, TextInput, Dimensions, FlatList, Modal, ImageBackground, ScrollView } from 'react-native';
import CONFIGURATION from '../../Reducer/Unills/Config';
import style from './../../Reducer/Styles/StoreImage'
import GeneralStatusBar from './../GeneralStatusBar'
import ShopView from './../ShopView'
import PagerView from 'react-native-pager-view'; 
const { width ,height} = Dimensions.get("screen")
class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Ex: [{ type: "Shop", color: "#FF725E", title: "Raj Moti Store", url: "https://image.shutterstock.com/image-photo/shopping-kids-during-virus-outbreak-260nw-1677576121.jpg" }, { type: "Offer", color: "#A47200", title: "Raj Moti Store", url: "https://media-cdn.tripadvisor.com/media/photo-s/06/17/c7/3f/item-shop.jpg" }, { type: "Offer", color: "#A0616A", title: "Raj Moti Store", url: "https://media-cdn.tripadvisor.com/media/photo-s/06/17/c7/3f/item-shop.jpg" }, { type: "More", color: "#735BF6", title: "Raj Moti Store", url: "https://image.shutterstock.com/image-photo/shopping-kids-during-virus-outbreak-260nw-1677576121.jpg" }],
            modalVisible: false,
            pageIndex: 0,
            url:"https://trak.in/wp-content/uploads/2020/10/images-2020-10-24T153811.037-2.jpeg"
        };
    }

    render() {
        return (
            <View style={style.container}>
                <GeneralStatusBar backgroundColor={CONFIGURATION.PrimaryColor} barStyle="light-content" />
                <View style={style.headerView}>
                    <TouchableOpacity onPress={() => { }}>
                        <Image resizeMode={"contain"} style={style.menuImage} source={require("./../Images/Back_W.png")} />
                    </TouchableOpacity>
                </View>
                <ScrollView>
                <View style={style.titleView}>
                    <Text style={style.headerText}>Select Store Category</Text>
                </View>
                <View style={{marginHorizontal: 20,flexDirection:"row",justifyContent:"space-between",marginTop:10}}>
                    <View style={{height:width/3-25,width:width/3-25,borderColor:CONFIGURATION.White+50,borderWidth:0.5,borderRadius:5,alignItems:"center",justifyContent:"center"}}>
                        <Image resizeMode={"contain"} style={{height:50,width:50}} source={require("./../Images/All_Shop.png")}/>
                        <Text style={{color:CONFIGURATION.White+50,fontFamily:CONFIGURATION.TextRegular,marginTop:5}}>All Shops</Text>
                    </View>
                    <View style={{height:width/3-25,width:width/3-25,borderColor:CONFIGURATION.White+50,borderWidth:0.5,borderRadius:5,alignItems:"center",justifyContent:"center"}}>
                        <Image resizeMode={"contain"} style={{height:50,width:50}} source={require("./../Images/grocery.png")}/>
                        <Text style={{color:CONFIGURATION.White+50,fontFamily:CONFIGURATION.TextRegular,marginTop:5}}>Grocery</Text>
                    </View>
                    <View style={{height:width/3-25,width:width/3-25,borderColor:CONFIGURATION.White+50,borderWidth:0.5,borderRadius:5,alignItems:"center",justifyContent:"center"}}>
                        <Image resizeMode={"contain"} style={{height:50,width:50}} source={require("./../Images/vegetables.png")}/>
                        <Text style={{color:CONFIGURATION.White+50,fontFamily:CONFIGURATION.TextRegular,marginTop:5}}>Fruit/Veg</Text>
                    </View>
                </View>
                <View style={{marginHorizontal: 20,flexDirection:"row",justifyContent:"space-between",marginTop:20}}>
                    <View style={{height:width/3-25,width:width/3-25,borderColor:CONFIGURATION.White+50,borderWidth:0.5,borderRadius:5,alignItems:"center",justifyContent:"center"}}>
                        <Image resizeMode={"contain"} style={{height:50,width:50}} source={require("./../Images/restaurant.png")}/>
                        <Text style={{color:CONFIGURATION.White+50,fontFamily:CONFIGURATION.TextRegular,marginTop:5}}>Eatery</Text>
                    </View>
                    <View style={{height:width/3-25,width:width/3-25,borderColor:CONFIGURATION.White+50,borderWidth:0.5,borderRadius:5,alignItems:"center",justifyContent:"center"}}>
                        <Image resizeMode={"contain"} style={{height:50,width:50}} source={require("./../Images/train.png")}/>
                        <Text style={{color:CONFIGURATION.White+50,fontFamily:CONFIGURATION.TextRegular,marginTop:5}}>Toys</Text>
                    </View>
                    <View style={{height:width/3-25,width:width/3-25,borderColor:CONFIGURATION.White+50,borderWidth:0.5,borderRadius:5,alignItems:"center",justifyContent:"center"}}>
                        <Image resizeMode={"contain"} style={{height:50,width:50}} source={require("./../Images/non_veg.png")}/>
                        <Text style={{color:CONFIGURATION.White+50,fontFamily:CONFIGURATION.TextRegular,marginTop:5}}>Non-Veg</Text>
                    </View>
                </View>
                <View style={{marginHorizontal: 20,flexDirection:"row",justifyContent:"space-between",marginTop:20}}>
                    <View style={{height:width/3-25,width:width/3-25,borderColor:CONFIGURATION.White+50,borderWidth:0.5,borderRadius:5,alignItems:"center",justifyContent:"center"}}>
                        <Image resizeMode={"contain"} style={{height:50,width:50}} source={require("./../Images/Salon.png")}/>
                        <Text style={{color:CONFIGURATION.White+50,fontFamily:CONFIGURATION.TextRegular,marginTop:5}}>Salon</Text>
                    </View>
                    <View style={{height:width/3-25,width:width/3-25,borderColor:CONFIGURATION.White+50,borderWidth:0.5,borderRadius:5,alignItems:"center",justifyContent:"center"}}>
                        <Image resizeMode={"contain"} style={{height:50,width:50}} source={require("./../Images/bakery.png")}/>
                        <Text style={{color:CONFIGURATION.White+50,fontFamily:CONFIGURATION.TextRegular,marginTop:5}}>Bakery</Text>
                    </View>
                    <View style={{height:width/3-25,width:width/3-25,borderColor:CONFIGURATION.White+50,borderWidth:0.5,borderRadius:5,alignItems:"center",justifyContent:"center"}}>
                        <Image resizeMode={"contain"} style={{height:50,width:50}} source={require("./../Images/Fashion.png")}/>
                        <Text style={{color:CONFIGURATION.White+50,fontFamily:CONFIGURATION.TextRegular,marginTop:5}}>Fashion</Text>
                    </View>
                </View>
                <View style={{marginHorizontal: 20,flexDirection:"row",justifyContent:"space-between",marginTop:20}}>
                    <View style={{height:width/3-25,width:width/3-25,borderColor:CONFIGURATION.White+50,borderWidth:0.5,borderRadius:5,alignItems:"center",justifyContent:"center"}}>
                        <Image resizeMode={"contain"} style={{height:50,width:50}} source={require("./../Images/Others.png")}/>
                        <Text style={{color:CONFIGURATION.White+50,fontFamily:CONFIGURATION.TextRegular,marginTop:5}}>Others</Text>
                    </View>
                </View>
                </ScrollView>
            </View>
        );
    }
}

export default App