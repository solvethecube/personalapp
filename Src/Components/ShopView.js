import React, { Component } from 'react';
import { Text, View, StyleSheet, Dimensions, ImageBackground, TouchableOpacity } from 'react-native';
import CONFIGURATION from './../Reducer/Unills/Config';
import LinearGradient from 'react-native-linear-gradient';

const { height, width } = Dimensions.get("screen")
class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Ex: ""
        };
    }

    render() {
        console.log('====================================');
        console.log(this.props.Url);
        console.log('====================================');
        return (
            <>
                {
                    this.props.type == "Shop" ?
                        <ImageBackground resizeMode="cover" source={{ uri: this.props.Url }} style={{ height: width / 3, width: width / 2 - 30, marginHorizontal: 10, marginTop: 20, borderRadius: 15, overflow: "hidden" }}>
                            <View style={{ height: 40, backgroundColor: CONFIGURATION.shopTitleBg, position: "absolute", bottom: 0, width: "100%", justifyContent: "center", paddingHorizontal: 10 }}>
                                <Text numberOfLines={1} style={{ color: CONFIGURATION.White, fontFamily: CONFIGURATION.TextRegular }} >{this.props.title}</Text>
                            </View>
                        </ImageBackground>
                        :
                        null
                }
                {
                    this.props.type == "Offer" ?
                        <TouchableOpacity onPress={this.props.onPress}>
                            <ImageBackground resizeMode="cover" source={{ uri: this.props.Url }} style={{ height: width / 3, width: width / 2 - 30, marginHorizontal: 10, marginTop: 20, borderRadius: 10, overflow: "hidden" }}>
                                <View style={{ height: 40, backgroundColor: CONFIGURATION.shopTitleBg, position: "absolute", bottom: 0, width: "100%", justifyContent: "center", paddingHorizontal: 10 }}>
                                    <Text numberOfLines={1} style={{ color: CONFIGURATION.White, fontFamily: CONFIGURATION.TextRegular }} >{this.props.title}</Text>
                                </View>
                                <View style={{ height: 36, width: 36, borderRadius: 10, backgroundColor: this.props.color, alignItems: "center", justifyContent: "center", alignSelf: "flex-end", borderColor: CONFIGURATION.White, borderWidth: 0.4 }}>
                                    <Text style={{ color: CONFIGURATION.White, fontFamily: CONFIGURATION.TextRegular, fontSize: 10, textAlign: "center" }}>{this.props.diss}{"%\nOFF"}</Text>
                                </View>
                            </ImageBackground>
                        </TouchableOpacity>
                        :
                        null
                }
                {
                    this.props.type == "More" ?
                        <TouchableOpacity onPress={this.props.onPress}>
                            <ImageBackground resizeMode="cover" source={{ uri: this.props.Url }} style={{ height: width / 3, width: width / 2 - 30, marginHorizontal: 10, marginTop: 20, borderRadius: 10, overflow: "hidden" }}>
                                <View style={{ height: 40, backgroundColor: CONFIGURATION.shopTitleBg, position: "absolute", bottom: 0, width: "100%", justifyContent: "center", paddingHorizontal: 10 }}>
                                    <Text numberOfLines={1} style={{ color: CONFIGURATION.White, fontFamily: CONFIGURATION.TextRegular }} >{this.props.title}</Text>
                                </View>
                                <LinearGradient colors={[CONFIGURATION.PrimaryColor, CONFIGURATION.PrimaryColor+99,"transparent"]} style={{height:40,width:"100%",paddingHorizontal:10}}>
                                    <Text numberOfLines={2} style={{ color: CONFIGURATION.White, fontFamily: CONFIGURATION.TextRegular }}>Get 500gm free with purchase of 3Kg </Text>
                                </LinearGradient>
                            </ImageBackground>
                        </TouchableOpacity>
                        :
                        null
                }
            </>
        );
    }
}

export default App