import { StyleSheet, Dimensions } from 'react-native';
import CONFIGURATION from "./../Unills/Config"

const { height, width } = Dimensions.get("screen");

export default StyleSheet.create({

    container: {
        backgroundColor: CONFIGURATION.PrimaryColor,
        flex: 1,
    },
    headerView: {
        flexDirection: "row",
        marginHorizontal: 20,
        marginVertical: 20,
        justifyContent: "space-between"
    },
    menuImage: {
        height: 25,
        width: 25,
    },
    searchImage: {
        height: 20,
        width: 20,
    },
    filterimage: {
        height: 20,
        width: 20,
        marginLeft:5
    },
    locationImage: {
        height: 20,
        width: 20,
    },
    bellImage: {
        height: 25,
        width: 25,
    },
    headerText:{
        fontSize:18,
        color:CONFIGURATION.White,
        fontFamily:CONFIGURATION.TextBold
    },
    titleView:{
        marginHorizontal:20,
        flexDirection:"row",
        justifyContent:"space-between",
        marginBottom:20,
        alignItems:"center"
    },
    locationView:{
        height:40,
        width:"45%",
        backgroundColor:CONFIGURATION.PrimaryLight,
        borderRadius:10,
        flexDirection:"row",
        alignItems:"center",
        paddingHorizontal:5,
    },
    textlocation:{
        color:CONFIGURATION.TitleColor+50,
        fontSize:13,
        fontFamily:CONFIGURATION.TextRegular,
        marginLeft:5
    },
    searchView:{
        height:45,
        marginHorizontal:20,
        backgroundColor:CONFIGURATION.PrimaryLight,
        borderRadius:10,
        flexDirection:"row",
        alignItems:"center",
    },
    input:{
        height:45,
        width:"90%",
        paddingHorizontal:10,
        color:CONFIGURATION.White,
        fontFamily:CONFIGURATION.TextRegular
    },
    filertView:{
        paddingVertical:20,
        paddingHorizontal:20,
        borderColor:CONFIGURATION.White+50,
        borderBottomWidth:1,
        flexDirection:"row",
        alignItems:"center",
        justifyContent:"space-between"
    },
    texts:{
        color:CONFIGURATION.White+50,
        fontSize:16
    },
    centeredView: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      backgroundColor: "#00000050",
    },
    modalView: {
      backgroundColor: CONFIGURATION.PrimaryColor,
      borderRadius: 20,
      shadowColor: CONFIGURATION.lightBlue,
      shadowOffset: {
        width: 0,
        height: 2
      },
      shadowOpacity: 0.25,
      shadowRadius: 4,
      elevation: 5,
      width:width-40
    },
    button: {
      borderRadius: 20,
      padding: 10,
      elevation: 2
    },
    buttonOpen: {
      backgroundColor: "#F194FF",
    },
    buttonClose: {
      backgroundColor: "#2196F3",
    },
    textStyle: {
      color: "white",
      fontWeight: "bold",
      textAlign: "center"
    },
    modalText: {
      marginBottom: 15,
      textAlign: "center"
    }
});