import { StyleSheet, Dimensions } from 'react-native';
import CONFIGURATION from "./../Unills/Config"

const {height , width} = Dimensions.get("screen");

export default StyleSheet.create({

  container: {
    backgroundColor: CONFIGURATION.PrimaryColor,
    flex: 1,
  },

});