const CONFIGURATION = {

    //Colors
    StatusBarColor: "#00000050",
    PrimaryColor:"#0B1D32",
    TitleColor:"#FAFAFA",
    PrimaryLight:"#1D2D4A",
    OrangeColor:"#FF725E",
    DarkGreen:"#A47200",
    LightPurpale:"#A0616A",
    shopTitleBg:"#15243BE5",
    lightBlue:"#41AEFB",
    bottomeBar:"#15182a",
    Black:"#000000",
    White:"#ffffff",
    
    // Font Family
    TextRegular: "Montserrat-Regular",
    TextBold: "Montserrat-Bold",
    TextMedium:"Montserrat-Medium",
}

export default CONFIGURATION