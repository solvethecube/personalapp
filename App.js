// React Navigate Drawer with Bottom Tab – Example using React Navigation V5 //
// https://aboutreact.com/bottom-tab-view-inside-navigation-drawer //
import 'react-native-gesture-handler';

import React, { Component } from 'react';
import { View, Image } from 'react-native';
import { NavigationContainer, getFocusedRouteNameFromRoute } from '@react-navigation/native';
import { createStackNavigator, CardStyleInterpolators } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import Home from './Src/Components/Pages/Home'
import HotDeals from './Src/Components/Pages/HotDeals'
import MoreDeals from './Src/Components/Pages/MoreDeals'
import Store from './Src/Components/Pages/Store'
import StoreImage from './Src/Components/Pages/StoreImage'
import Category from './Src/Components/Pages/Category'

import {
  Dimensions
} from 'react-native';
import CONFIGURATION from './Src/Reducer/Unills/Config';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

function HomeTab() {
  return (
    <Tab.Navigator 
    
    screenOptions={{tabBarLabelStyle:{fontFamily:CONFIGURATION.TextRegular,fontSize:13},headerShown :false,tabBarInactiveTintColor:CONFIGURATION.White ,tabBarActiveTintColor:CONFIGURATION.lightBlue,tabBarStyle:{backgroundColor:CONFIGURATION.bottomeBar,height:70,paddingBottom:10,borderTopLeftRadius:20,borderTopRightRadius:20}}}>
      <Tab.Screen
        name="Home"
        component={Category}
        options={{
          tabBarIcon: ({color, size}) => {
            return ( 
              <Image resizeMode={"contain"} style={{height:25,width:25,tintColor:color}} source={require("./Src/Components/Images/Home.png")} />
            )
          }
        }}
      />
      <Tab.Screen
        name="MyWealth"
        component={HotDeals}
        options={{
          tabBarIcon: ({color, size}) => {
            return (
              <Image resizeMode={"contain"} style={{height:25,width:25,tintColor:color}} source={require("./Src/Components/Images/wealth.png")} />
            )
          }
        }}
      />
      <Tab.Screen
        name="Nearby Stores"
        component={Home}
        options={{
          tabBarIcon: ({color, size}) => {
            return (
              <Image resizeMode={"contain"} style={{height:25,width:25,tintColor:color}} source={require("./Src/Components/Images/storys.png")} />
            )
          }
        }}
      />
      <Tab.Screen
        name="Rewards"
        component={Home}
        options={{
          tabBarIcon: ({color, size}) => {
            return (
              <Image resizeMode={"contain"} style={{height:25,width:25,tintColor:color}} source={require("./Src/Components/Images/Rewards.png")} />
            )
          }
        }}
      />
    </Tab.Navigator>
  );
  }


export default class App extends Component {
  constructor(properties) {
    super(properties);
    this.state = {
      buySub: 0
    };
  }
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator
          screenOptions={{
            headerShown: false
          }}
        >
          <Stack.Screen
            name="Home"
            component={HomeTab}
            options={{ cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS }}
          />
          <Stack.Screen
            name="MoreDeals"
            component={MoreDeals}
            options={{ cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS }}
          />
          <Stack.Screen
            name="Store"
            component={Store}
            options={{ cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS }}
          />
          <Stack.Screen
            name="StoreImage"
            component={StoreImage}
            options={{ cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}